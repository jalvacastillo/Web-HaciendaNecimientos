@extends('base')

@section('titulo')
    Servicios
@endsection

@section('content')


<div class="fh5co-hero fh5co-hero-2">
    <div class="fh5co-overlay"></div>
    <div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/blog-1.jpg);">
        <div class="desc animate-box">
            <h2>Nuestros <strong>Tours</strong></h2>
            <span>Conoce con nosotros</span>
        </div>
    </div>
</div>
<!-- end:header-top -->

<div id="fh5co-services-section" class="fh5co-section-gray">
     <div class="container">
        <div class="row text-center">
            <div class="col-md-4 col-sm-4">
                <div class="services animate-box">
                    <img src="{{ asset('/images/tour/agro.jpg') }}" alt="" width="100%">
                    <h3>Agroecoturismo</h3>
                    <p>Disfrute el turismo en una forma diferente, que descubra el olor a campo por los caminos de los frutos.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="services animate-box">
                    <img src="{{ asset('/images/tour/ruta_azul.jpg') }}" alt="" width="100%">
                    <h3>Ruta Azul Maya</h3>
                    <p>Los Νacimientos le ofrece servicios de tours guiados sobre la cultura e historia de la ruta Azul Maya.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="services animate-box">
                    <img src="{{ asset('/images/tour/aves.jpg') }}" alt="" width="100%">
                    <h3>Avistamiento de Aves</h3>
                    <p>Si lo que quieres es disfrutar observando aves de plumaje o canto en nuestra hacienda contamos con recorridos que te dejarán una experiencia única.</p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center animate-box">
                    <a href="{{ route('contactos') }}" class="btn btn-success btn-lg">Reserva</a>
                </div>
            </div>

        </div>
    </div>
    </div>
</div>

@endsection