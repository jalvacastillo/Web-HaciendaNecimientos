@extends('base')

@section('titulo')
    Productos
@endsection

@section('content')

    <div class="fh5co-hero fh5co-hero-2">
        <div class="fh5co-overlay"></div>
        <div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/work-3.jpg);">
            <div class="desc animate-box">
                <h2>Nuestros <strong>Productos</strong></h2>
                <span>Te ofrecemos productos agro-industriales.</span>
            </div>
        </div>

    </div>
    <!-- end:header-top -->
    <div id="fh5co-blog-section" class="fh5co-section-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
                    <h3>Productos</h3>
                    <p>Productos 100% naturales, cultivados, procesados y comercializados con estándares de calidad.</p>
                </div>
            </div>
        </div>
        <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="{{ asset('images/productos/anil.jpg') }}" alt=""></a>
                            <div class="blog-text">
                                <div class="prod-title">
                                    <h3><a href=""#>Añil</a></h3>
                                    {{-- <span class="posted_by">Quintal</span> --}}
                                    {{-- <span class="comment"><a href="">$19<i class="icon-bubble2"></i></a></span> --}}
                                    <p>Somos pioneros en el cultivo de añil orgánico certificado por <a href="http://www.biolatina.com" target="blank">Bio Latina</a>, usado en la industria textil y farmacéutica en países como  Alemania, Brasil, entre otros.</p>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="{{ asset('images/productos/maranon.jpg') }}" alt=""></a>
                            <div class="blog-text">
                                <div class="prod-title">
                                    <h3><a href=""#>Semilla Marañón</a></h3>
                                    <p>Otro de los frutos de nuestra hacienda es la semilla de marañón, de la cual se produce vino y otros productos derivados como jaleas y siropes.</p>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="{{ asset('images/productos/textil.jpg') }}" alt=""></a>
                            <div class="blog-text">
                                <div class="prod-title">
                                    <h3><a href=""#>Prendas teñidas en añil</a></h3>
                                    <p>Teñimos diferentes prendas de vestir, accesorios para el hogar, cocina, oficina y artículos decorativos para diferentes eventos. </p>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="{{ asset('images/productos/moringa.jpg') }}" alt=""></a>
                            <div class="blog-text">
                                <div class="prod-title">
                                    <h3><a href=""#>Aceite de Moringa</a></h3>
                                    <p>De las semillas de la planta de moringa se elaboran aceites esenciales para el cuidado y belleza de la piel.</p>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="{{ asset('images/productos/jamaica.jpg') }}" alt=""></a>
                            <div class="blog-text">
                                <div class="prod-title">
                                    <h3><a href=""#>Flor de Rosa de Jamaica</a></h3>
                                    <p>Cultivamos y vendemos flor de rosa de jamaica deshidratada y elaboramos productos derivados como siropes, vinos y jaleas.  </p>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="{{ asset('images/productos/vinos.jpg') }}" alt=""></a>
                            <div class="blog-text">
                                <div class="prod-title">
                                    <h3><a href=""#>Vinos</a></h3>
                                    <p>Elaboramos vinos de jamaica y marañón.</p>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-md-offset-4 text-center animate-box">
                        <a href="{{ route('contactos') }}" class="btn btn-success btn-lg">Comprar</a>
                    </div>
                </div>

            </div>
    </div>

@endsection