<div class="fh5co-hero">
    <div class="fh5co-overlay"></div>
    <div class="fh5co-cover text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/cover_bg_1.jpg);">
        <div class="desc animate-box">
            <img src="/images/logo.png" alt="" style="margin-top:-150px;" width="300">
            <h2>Hacienda <strong>los nacimientos</strong></h2>
            <span>AGRO-ECOTURISMO | AGRO-INDUSTRIA | CENTRO DE FORMACIÓN</span>
            <a class="btn btn-success" href="{{ route('productos') }}">Productos</a>
            <a class="btn btn-success" href="{{ route('servicios') }}">Servicios</a>
        </div>
    </div>

</div>