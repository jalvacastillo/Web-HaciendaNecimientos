<div id="fh5co-feature-product">
    <div class="container">
        <div class="row row-bottom-padded-md">
            <div class="col-md-12 text-center heading-section">
                <h3>Quieres aprender todo sobre el añil</h3>
                <p>Te ayudamos a ser un experto en la producción, procesamiento y uso del añil.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="feature-text">
                    <h3><span class="number">01.</span> Brindamos consultorías</h3>
                    <p>La experiencia en el cultivo orgánico del añil nos permite compartir el conocimiento sobre su cultivo sin importar la extensión de tierra en la que quieras invertir.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-text">
                    <h3><span class="number">02.</span> Impartimos clases</h3>
                    <p>Si quieres aprender las diferentes técnicas de teñidos en añil contamos con un programa de enseñanza básico, intermedio y avanzado.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-text">
                    <h3><span class="number">03.</span> Conferencias y foros.</h3>
                    <p>Somos los referentes de Centro América en la re activación del añil para la industria de tintes naturales, hemos participado en ponencias a nivel mundial.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center animate-box">
                <a href="{{ route('servicios') }}" class="btn btn-success btn-lg">Conoce más</a>
            </div>
        </div>

        
    </div>
</div>