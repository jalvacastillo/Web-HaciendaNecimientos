<div id="fh5co-services-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
                <h3>Tours</h3>
                <p>Disfruta de nuestros increibles tours.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-4 col-sm-4">
                <div class="fh5co-blog animate-box">
                    <a href="#"><img class="img-responsive" src="{{ asset('/images/tour/agro.jpg') }}" alt=""></a>
                    <div class="blog-text">
                        <div class="prod-title">
                            <h3><a href=""#>Agroecoturismo</a></h3>
                            <p>Disfrute el turismo en una forma diferente, descubre el olor a campo por los caminos de los ...</p>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="fh5co-blog animate-box">
                    <a href="#"><img class="img-responsive" src="{{ asset('/images/tour/ruta_azul.jpg') }}" alt=""></a>
                    <div class="blog-text">
                        <div class="prod-title">
                            <h3><a href=""#>Ruta Azul Maya</a></h3>
                            <p>Los Νacimientos le ofrece servicios de tours guiados sobre la cultura e historia de la ruta Azul Maya...</p>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="fh5co-blog animate-box">
                    <a href="#"><img class="img-responsive" src="{{ asset('/images/tour/aves.jpg') }}" alt=""></a>
                    <div class="blog-text">
                        <div class="prod-title">
                            <h3><a href=""#>Avistamiento de Aves</a></h3>
                            <p>Si lo que quieres es disfrutar observando aves de plumaje o canto contamos con recorridos...</p>
                        </div>
                    </div> 
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center animate-box">
                    <a href="{{ route('tours') }}" class="btn btn-success btn-lg">Ver tours</a>
                </div>
            </div>

        </div>
    </div>
</div>