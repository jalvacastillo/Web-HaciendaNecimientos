<div id="fh5co-blog-section" class="fh5co-section-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
                <h3>Productos</h3>
                <p>Te ofrecemos variedad de productos agro-industriales.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row row-bottom-padded-md">
            <div class="col-lg-4 col-md-4">
                <div class="fh5co-blog animate-box">
                    <a href="#"><img class="img-responsive" src="{{ asset('images/productos/anil.jpg') }}" alt=""></a>
                    <div class="blog-text">
                        <div class="prod-title">
                            <h3><a href=""#>Añil</a></h3>
                            {{-- <span class="posted_by">Quintal</span> --}}
                            {{-- <span class="comment"><a href="">$19<i class="icon-bubble2"></i></a></span> --}}
                            <p>Somos pioneros en el cultivo de añil orgánico certificado por <a href="http://www.biolatina.com" target="blank">Bio Latina</a>, usado en la industria textil y farmacéutica en países como  Alemania, Brasil, entre otros.</p>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="fh5co-blog animate-box">
                    <a href="#"><img class="img-responsive" src="{{ asset('images/productos/textil.jpg') }}" alt=""></a>
                    <div class="blog-text">
                        <div class="prod-title">
                            <h3><a href=""#>Prendas teñidas en añil</a></h3>
                            {{-- <span class="posted_by">Bolsa</span> --}}
                            {{-- <span class="comment"><a href="">$10<i class="icon-bubble2"></i></a></span> --}}
                            <p>Teñimos diferentes prendas de vestir, accesorios para el hogar, cocina, oficina y artículos decorativos para diferentes eventos. </p>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="fh5co-blog animate-box">
                    <a href="#"><img class="img-responsive" src="{{ asset('images/productos/jamaica.jpg') }}" alt=""></a>
                    <div class="blog-text" style="min-height: 235px;">
                        <div class="prod-title">
                            <h3><a href=""#>Flor de Rosa de Jamaica</a></h3>
                            <p>Cultivamos flor de rosa de jamaica y elaboramos productos derivados como siropes, vinos y jaleas.  </p>
                        </div>
                    </div> 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center animate-box">
                <a href="{{ route('productos') }}" class="btn btn-success btn-lg">Ver todos los productos</a>
            </div>
        </div>

    </div>
</div>