@extends('base')

@section('titulo')
    Contactos
@endsection

@section('content')

    <div class="fh5co-hero fh5co-hero-2">
        <div class="fh5co-overlay"></div>
        <div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/blog-2.jpg);">
            <div class="desc animate-box">
                <h2><strong>Comunicate</strong> con nosotros</h2>
                <span>Reserva tours <br> Solicita nuestros productos o servicios</span>
            </div>
        </div>
    </div>
    <!-- end:header-top -->

    <div id="fh5co-contact" class="animate-box" ng-app="app">
        <div class="container">
            <form ng-controller="CorreoCtrl" role="form" method="POST" ng-submit="email(correo);">
                <div class="row">
                    <div class="col-md-6">
                         <h3 class="section-title">Escríbenos</h3>
                        <p>Estaremos encantados de conocerte</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input ng-model="correo.nombre" type="text" class="form-control" placeholder="Nombre" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input ng-model="correo.correo" type="email" class="form-control" placeholder="Correo" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea ng-model="correo.mensaje" class="form-control" id="" cols="30" rows="3" placeholder="Describe lo que necesitas..." required></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <button class="btn btn-success" type="submit" name="action">
                                      <span ng-show="!loader">Enviar</span>
                                      <span ng-show="loader">Enviando...</span>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-12 text-center" ng-show="errores">
                                <div class="alert alert-warning" role="alert">
                                    <p> Upps... No se pudo enviar el correo, intente nuevamente. </p>
                                </div>
                            </div>
                            <div class="col-md-12 text-center" ng-show="enviado">
                                <div class="alert alert-success" role="alert">
                                    <p> Correo enviado, Nos contactaremos lo antes posible con usted!!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3 class="section-title">Nuestra dirección</h3>
                        <p>Estamos ubicados en El Salvador, cantón San Lucas, municipio de Suchitoto a 45 Km. de la ciudad capital. A solo 10 Km. del sitio arqueológico CIHUATAN y a 17 Km. de la ciudad capital de Suchitoto.</p>
                        <ul class="contact-info">
                            <li><i class="icon-location-pin"></i>Canton San Lucas, Suchitoto, El Salvador.</li>
                            <li><i class="icon-phone2"></i><a href="tel:7885-8718" >Tel.: (503) 2243-0449 / Cel.: (503) 7885-8718</a></li>
                            {{-- <li><i class="icon-mail"></i><a href="mailto:info@haciendalosnacimientos.com">info@haciendalosnacimientos.com</a></li> --}}
                            <li><i class="icon-mail"></i><a href="mailto:rhina.rehemann@gmail.com">rhina.rehemann@gmail.com</a></li>
                            <li><i class="icon-chat"></i><a href="https://m.me/HaciendaLosNacimientos" target="blank">Escríbenos a Messenger</a></li>
                        </ul>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
    <!-- END fh5co-contact -->
    <div id="map" class="fh5co-map"></div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
    <script src="/js/google_map.js"></script>

@endsection