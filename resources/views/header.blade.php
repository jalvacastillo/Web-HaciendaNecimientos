<div id="fh5co-header">
    <header id="fh5co-header-section">
        <div class="container">
            <div class="nav-header">
                <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                {{-- <h1 id="fh5co-logo"><a href="{{ url('/') }}">Nacimientos</a></h1> --}}
                <!-- START #fh5co-menu-wrap -->
                <nav id="fh5co-menu-wrap" role="navigation">
                    <ul class="sf-menu" id="fh5co-primary-menu">
                        <li class="{{ request()->is('/') ? 'active' : '' }}"> <a href="{{ route('home') }}">Inicio</a> </li>
                        <li class="{{ request()->is('nosotros') ? 'active' : '' }}"><a href="{{ route('nosotros') }}">Nosotros</a></li>
                        <li class="{{ request()->is('tours') ? 'active' : '' }}"><a href="{{ route('tours') }}">Tours</a></li>
                        <li class="{{ request()->is('servicios') ? 'active' : '' }}"><a href="{{ route('servicios') }}">Servicios</a></li>
                        <li class="{{ request()->is('productos') ? 'active' : '' }}"><a href="{{ route('productos') }}">Productos</a></li>
                        <li class="{{ request()->is('contactos') ? 'active' : '' }}"><a href="{{ route('contactos') }}">Contactos</a></li>
                       {{--  <li>
                            <a href="portfolio.html" class="fh5co-sub-ddown">Projects</a>
                            <ul class="fh5co-sub-menu">
                                <li><a href="http://freehtml5.co/preview/?item=build-free-html5-bootstrap-template" target="_blank">Build</a></li>
                                <li><a href="http://freehtml5.co/preview/?item=work-free-html5-template-bootstrap" target="_blank">Work</a></li>
                                <li><a href="http://freehtml5.co/preview/?item=light-free-html5-template-bootstrap" target="_blank">Light</a></li>
                                <li><a href="http://freehtml5.co/preview/?item=relic-free-html5-template-using-bootstrap" target="_blank">Relic</a></li>
                                <li><a href="http://freehtml5.co/preview/?item=display-free-html5-template-using-bootstrap" target="_blank">Display</a></li>
                                <li><a href="http://freehtml5.co/preview/?item=sprint-free-html5-template-bootstrap" target="_blank">Sprint</a></li>
                            </ul>
                        </li> --}}
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    
</div>