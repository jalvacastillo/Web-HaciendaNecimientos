<!DOCTYPE html>
<html class="no-js">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('titulo') - Hacienda Los Nacimientos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Hacienda Los Nacimientos, Productores de Añil, Ubicados En Suchitoto El Salvador. Recorre la ruta azul maya y Nuestro Recorrido Agroecoturistico. Hacienda 100% Amigable con la naturaleza." />
    <meta name="keywords" content="Suchitoto, Añil, Turismo El Salvador, Lugares Turisticos." />
    <meta name="author" content="CDMYPE - Jesus Alvarado" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" type='image/x-icon' href="{{ asset('/favicon.ico') }}">

    <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'> -->
    
    <!-- Animate.css -->
    <link rel="stylesheet" href="{{ asset('/css/animate.css') }}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{ asset('/css/icomoon.css') }}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
    <!-- Superfish -->
    <link rel="stylesheet" href="{{ asset('/css/superfish.css') }}">

    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">

    <script src="{{ asset('js/modernizr-2.6.2.min.js') }}"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117749359-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-117749359-1');
    </script>


    </head>
    <body>
        <div id="fh5co-wrapper">
            <div id="fh5co-page">
            
                @include('header')
                    @yield('content')
                @include('footer')
            
            </div>
        </div>

    <!-- jQuery -->
    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <!-- jQuery Easing -->
    <script src="{{ asset('/js/jquery.easing.1.3.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('/js/angular.min.js') }}"></script>
    <script src="{{ asset('/js/controller.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <!-- Waypoints -->
    <script src="{{ asset('/js/jquery.waypoints.min.js') }}"></script>
    <!-- Stellar -->
    <script src="{{ asset('/js/jquery.stellar.min.js') }}"></script>
    <!-- Superfish -->
    <script src="{{ asset('/js/hoverIntent.js') }}"></script>
    <script src="{{ asset('/js/superfish.js') }}"></script>

    <!-- Main JS -->
    <script src="{{ asset('/js/main.js') }}"></script>

    </body>
</html>

