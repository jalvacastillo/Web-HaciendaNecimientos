<footer>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <p class="fh5co-social-icons">
                        {{-- <a href="#"><i class="icon-twitter2"></i></a> --}}
                        <a href="https://www.facebook.com/HaciendaLosNacimientos" target="blank"><i class="icon-facebook2"></i></a>
                        {{-- <a href="https://www.instagram.com/HaciendaLosNacimientos" target="blank"><i class="icon-instagram"></i></a> --}}
                        {{-- <a href="#"><i class="icon-dribbble2"></i></a> --}}
                        {{-- <a href="#"><i class="icon-youtube"></i></a> --}}
                    </p>
                    <p> Cantón San Lucas, Suchitoto a 45 Km. de San Salvador. <p>Copyright 2018 - Derechos Reservados. <br>Por <i class="icon-heart3"></i> <a href="cri.catolica.edu.sv/cdmype" target="_blank">CDMYPE Ilobasco</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>