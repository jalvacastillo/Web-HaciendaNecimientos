<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hola</title>
</head>
<style>
    body{
        border-collapse:collapse!important;
        background-color:#f2f2f2;
        font-family: helvetica, sans-serif;
        height:100%!important;
        padding:0;
        width:100%!important;
        text-align: center;
    }

    .main  {
        margin: auto;
        background-color: #ffffff;
        width: 80%;
        padding: 20px 50px;
    }
</style>
<body>
    
    <div class="main">
        <img src="{{asset('images/logo.jpg')}}" style="width:25%;"></a>
        
        <h2>Saludos!</h2>
        
        @foreach($data as $clave=>$valor)
        <p> <b>{{ strtoupper($clave) }}</b> {{$valor}}</p>
        @endforeach

        <p style="margin-bottom:1em;font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;text-align:center;font-size:12px;line-height:1.34em;color:#999999;display:block" align="center">
        Hacienda los nacimientos &nbsp; &copy; &nbsp; &nbsp;2018, &nbsp; - &nbsp;&nbsp;Suchitoto
        <br>
                   
    </div>
                  
</body>
</html>