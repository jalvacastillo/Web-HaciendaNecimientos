<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<style>
    .main{
        border-collapse:collapse!important;
        background-color:#f2f2f2;
        font-family:sans-serif;
        height:100%!important;
        margin:0;
        padding:0;
        width:100%!important;
    }
</style>
<body>
    

<table align="center" bgcolor="#f2f2f2" border="0" cellpadding="0" cellspacing="0" height="100% !important" width="100% !important" id="main">
    <tbody>

        <tr>
            <td align="center" valign="top" style="padding:10px 20px;background-color:#f2f2f2" bgcolor="#f2f2f2">
              
              
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:separate!important;background-color:#ffffff;padding:0" bgcolor="#ffffff">
            <tbody>
                
                <tr>
                <td align="center" valign="top">
                    
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse!important;width:500px" width="500">
                      <tbody>

                        {{-- Header --}}
                        <tr>
                          <td align="left" valign="top" width="100%" colspan="12" style="color:#444444;font-family:sans-serif;font-size:15px;line-height:150%;text-align:left">
                            
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse!important">
                              <tbody>
                              <tr>
                                <td align="left" valign="top" colspan="12" width="100.0%" style="text-align:left;font-family:sans-serif;font-size:15px;line-height:1.5em;color:#444444">
                                <div style="text-align: center; color:inherit;font-size:inherit;line-height:inherit">
                                      <a href="">
                                      <img src="wish/img/logo.png" style="vertical-align:bottom;max-height:200px;max-width:600px;border-width:0px;border:0px" width="100" alt="Logotipo" title="Logotipo"></a>
                                      <h2>Tu sucursal online esta activa!</h2>
                                      <h5>Estamos listos para enseñarte como usarla</h5>
                                </div>
                                  
                                </td>
                              </tr>
                            </tbody>
                            </table>

                          </td>
                        </tr>
                        {{-- Info --}}
                        <tr>
                            <td align="left" valign="top" width="100%" colspan="12" style="color:#444444;font-family:sans-serif;font-size:15px;line-height:150%;text-align:left">
                            <table cellpadding="10" cellspacing="0" border="0" width="100%" style="border-collapse:collapse!important">
                              <tbody>
                                <tr>
                                    <td align="left" valign="top" colspan="12" width="100.0%" style="text-align:left;font-family:sans-serif;font-size:15px;line-height:1.5em;color:#444444">

                                    <div style="padding:10px; color:inherit;font-size:inherit;line-height:inherit">

                                        <p><b>Estos son tus datos:</b></p>
                                        <p style="margin:2px;">Tienda: {{ $data['nombre'] }}</p>
                                        <p style="margin:2px;">Correo: {{ $data['email'] }}</p>
                                        <p style="margin:2px;">Contraseña: {{ $data['password'] }}</p>
                                        
                                        <br><br>
                                        {{-- Booton --}}
                                        <table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse!important">
                                            <tbody>
                                            <tr>
                                            <td>

                                            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse!important">
                                            <tbody>
                                            <tr>
                                            <td style="padding:12px 18px 12px 18px;border-radius:3px;" align="center" bgcolor="#AD4A4A">
                                                <a href="{{ url('/login') }}" style="color: white;">Iniciemos</a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                                </tr>
                            </tbody>
                            </table>
                                  
                            </td>
                        </tr>
                    </tbody>
                    </table>
                </td>
                </tr>
                      
                      
            </tbody>
            </table>
            
            </td>
        </tr>
</tbody>
</table>
              

<table border="0" cellpadding="0" cellspacing="0" width="500" style="border-collapse:collapse!important;background-color:#f2f2f2;width:500px;color:#999999;font-family:sans-serif;font-size:12px;line-height:120%;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;text-align:center" bgcolor="#f2f2f2" align="center">
    <tbody>
        <tr>
        <td align="left" valign="top" width="100%" colspan="12" style="color:#444444;font-family:sans-serif;font-size:15px;line-height:150%;text-align:left">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse!important">
                <tbody><tr>
                <td align="left" valign="top" colspan="12" width="100.0%" style="text-align:left;font-family:sans-serif;font-size:15px;line-height:1.5em;color:#444444">
                <div>
                <p style="margin-bottom:1em;font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;text-align:center;font-size:12px;line-height:1.34em;color:#999999;display:block" align="center">
                Whispage &copy; 2016
                <br>
                <br> Plataforma de pedidos online.
                <br>
                <br>
                </p>

                </div>

                </td>
                </tr>
                </tbody>
            </table>
                  
        </td>
        </tr>

    </tbody>
</table>
              
</body>
</html>