@extends('base')

@section('titulo')
    Nosotros
@endsection

@section('content')

    <div class="fh5co-hero fh5co-hero-2">
        <div class="fh5co-overlay"></div>
        <div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/about-image.jpg);">
            <div class="desc animate-box">
                <h2><strong>Conocenos</strong> somos</h2>
                <span>Hacienda los Nacimientos</span>
            </div>
        </div>
    </div>
    <!-- end:header-top -->
    <div id="fh5co-about">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-md-offset-2 text-center heading-section animate-box">
                    <h3>Acerca de Nosotros</h3>
                    <p> Hacienda Los Νacimientos nace en 1995 con la visión de sobresalir en el área de la agroindustria rural, posicionándose a partir de ese momento como un líder competitivo, portador de salud, trabajo y bienestar social a todos sus allegados.</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-5 animate-box">
                    <figure>
                        <img src="{{ asset('images/nosotros/equipo.jpg') }}" alt="Free HTML5 Bootstrap Template by FREEHTML5.co" class="img-responsive">
                    </figure>
                </div>
                <div class="col-md-7 animate-box">
                    Tenemos como visión desarrollar y explotar eficientemente sus recursos naturales y sus procesos agro-industriales respetando el medio ambiente, mediante la implementación de técnicas de agricultura orgánica para ofrecer productos y servicios competitivos, con un alto valor agregado a los mercados locales e internacionales, trabajando con un personal eficaz y comprometido con los objetivos de la empresa, a través de un trato digno y justo.
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 animate-box pull-right">
                    <figure>
                        <img src="{{ asset('images/nosotros/cultivo.jpg') }}" alt="Free HTML5 Bootstrap Template by FREEHTML5.co" class="img-responsive">
                    </figure>
                </div>
                <div class="col-md-7 animate-box pull-left">
                    A partir de 1995 la empresa se ha constituido en un pilar importante para el desarrollo de la agricultura orgánica en el país.
                    <br><br>
                    Hemos invertido en la investigación de la agricultura tropical sostenible, como una forma de potenciar el valor agregado de sus cultivos y productos, ha implementado normas de producción orgánica y buenas prácticas agrícolas y de manufactura, obteniendo así condiciones vitales para competir en el actual mundo globalizado y poder llegar a ser auto sostenible e innovador.
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 animate-box">
                    <figure>
                        <img src="{{ asset('images/nosotros/expo.jpg') }}" alt="Free HTML5 Bootstrap Template by FREEHTML5.co" class="img-responsive">
                    </figure>
                </div>
                <div class="col-md-7 animate-box">
                    La empresa busca relaciones comerciales estratégicas con empresas europeas y americanas, en el sector de ingredientes naturales para la industria de cosméticos, alimentos y farmacéuticas.
                    <br><br>
                    Nos especializamos en la producción de polvo de añil orgánico de alta calidad con un porcentaje de índigotina de entre 50-55%.
                    <br><br>
                    <strong>Actualmente se exporta el polvo de añil a Alemania, Francia, Perú y Turquía.</strong>
                </div>

            </div>
        </div>
    </div>


@endsection