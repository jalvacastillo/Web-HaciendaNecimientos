@extends('base')

@section('titulo')
    Bienvenidos a la cuna del añil
@endsection

@section('content')


    @include('home.banner')
    @include('home.tours')
    @include('home.productos')
    @include('home.servicios')
    {{-- @include('home.blog') --}}
    {{-- @include('home.clientes') --}}

@endsection