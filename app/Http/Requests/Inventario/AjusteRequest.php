<?php

namespace App\Http\Requests\Inventario;

use Illuminate\Foundation\Http\FormRequest;

class AjusteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'producto_id'    => 'required:numeric',
            'lugar_id'      => 'required:numeric',
            'cantidad_inicial'    => 'required:numeric',
            'cantidad_final'    => 'required:numeric',
            'usuario_id'    => 'required:numeric'
        ];
    }
}
