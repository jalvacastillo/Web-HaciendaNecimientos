<?php

namespace App\Http\Requests\Otros;

use Illuminate\Foundation\Http\FormRequest;

class BombaCorteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bomba_id'    => 'required:numeric',
            'a'      => 'required:numeric',
            'd'    => 'required:numeric'
        ];
    }
}
