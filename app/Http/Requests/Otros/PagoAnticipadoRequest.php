<?php

namespace App\Http\Requests\Otros;

use Illuminate\Foundation\Http\FormRequest;

class PagoAnticipadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'fecha'    => 'required',
            'total'    => 'required',
            'tipo'    => 'required',
            'cliente_id'    => 'required',
            'usuario_id'    => 'required'
        ];
    }
}
