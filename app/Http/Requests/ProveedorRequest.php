<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProveedorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'    => 'required',
            // 'registro'  => 'unique:proveedores,registro,' . $this->id,
            // 'dui'    => 'unique:proveedores,dui,' . $this->id,
            // 'nit'    => 'unique:proveedores,nit,' . $this->id
        ];
    }
}
