<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Request;
use Carbon\Carbon;
use Mail;
use File;

class HomeController extends Controller
{
    

    public function index(){

        return view('index');
    }

    public function contactos(){

        return view('contactos.index');
    }

    public function productos(){

        return view('productos.index');
    }
    public function nosotros(){

        return view('nosotros.index');
    }

    public function servicios(){

        return view('servicios.index');
    }

    public function tours(){

        return view('tours.index');
    }

    public function servicio($id){

        $json = File::get("data/servicios.json");

        $array = json_decode($json, true);

        $servicios = collect($array[$id-1]);

        return view('servicios.index', compact('servicios'));
    }


    public function correo()
    {        
        try {
            $data = Request::all();

            Mail::send('emails.contacto', ['data' => $data], function ($m) use ($data) {
                $m->from('rhina.rehemann@gmail.com', 'Pagina Web')
                ->to('rhina.rehemann@gmail.com', 'Hacienda Los Nacimientos.')
                ->cc('jesus.alvarado@catolica.edu.sv')
                ->subject('Página Web');
            });
            return response()->json(['msj' => $data]);
        } catch (Exception $e) {
            return response()->json(['msj' => $data]);
        }

    }

}
