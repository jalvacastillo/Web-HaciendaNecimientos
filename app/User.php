<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'tipo', 'avatar', 'activo', 'caja_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['ventas', 'compras', 'anticipos', 'caja'];


    public function getVentasAttribute(){
        $ventas = $this->ventas()->get();
        return $ventas->sum('total');
    }

    public function getComprasAttribute(){
        $compras = $this->compras()->get();
        return $compras->sum('total');
    }

    public function getAnticiposAttribute(){
        return $this->anticipos()->sum('total');
    }

    public function getCajaAttribute(){
        return $this->caja()->pluck('nombre')->first();
    }

    public function caja(){
        return $this->belongsTo('App\Models\Admin\Caja', 'caja_id');
    }

    public function cortes(){
        return $this->hasMany('App\Models\Otros\Corte');
    }

    public function ventas(){
        return $this->hasMany('App\Models\Ventas\Venta', 'usuario_id');
    }

    public function compras(){
        return $this->hasMany('App\Models\Compras\Compra', 'usuario_id');
    }

    public function anticipos(){
        return $this->hasMany('App\Models\Otros\PagoAnticipado', 'usuario_id');
    }

    
}
