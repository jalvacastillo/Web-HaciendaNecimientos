<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmpresaTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
        $this->call(ProductosTableSeeder::class);
        $this->call(BodegasTableSeeder::class);
        $this->call(BodegaProductosTableSeeder::class);
        $this->call(TanquesTableSeeder::class);
        $this->call(BombasTableSeeder::class);
        $this->call(CajasTableSeeder::class);
        
        $this->call(ClientesTableSeeder::class);
        $this->call(ProveedoresTableSeeder::class);

        // $this->call(PagosAdelantadosTableSeeder::class);
        // $this->call(VentasTableSeeder::class);
        // $this->call(VentaDetallesTableSeeder::class);
        // $this->call(ComprasTableSeeder::class);
        // $this->call(CompraDetallesTableSeeder::class);
        

    }
}
