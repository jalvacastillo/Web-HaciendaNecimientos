"use strict";

angular.module('app', [ ])

.constant("config", {
    "url": "http://haciendalosnacimientos.com/"
    // "url": "http://localhost:8000/"
})

.factory('Dateme', [function () {
    
    function get(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth();
        var yyyy = today.getFullYear();
        var date = new Date(yyyy, mm, dd);
        return date;
    };

    return {
        get : get
    };

}])

.controller('ToursCtr', ['$scope', '$http', 'config', function ($scope, $http, config) {

    $scope.tours = [];

    $http.get('/data/tours.json').then(
        function(data){ 
            $scope.tours = data.data;
        }, function(data){
            console.log(data);
        }
    );
        
}])


.controller('GaleriaCtrl', ['$scope', '$http', 'config', function ($scope, $http, config) {

    $http.get('/data/imagenes.json').then(
        function(data){ 
            $scope.imagenes = data.data;
        }, function(data){
            console.log(data);
        }
    );
        
}])

.controller('ClientesCtrl', ['$scope', '$http', 'config', function ($scope, $http, config) {

    $scope.clientes = [];

    $http.get('/data/clientes.json').then(
        function(data){ 
            $scope.clientes = data.data;
        }, function(data){
            console.log(data);
        }
    );
        
}])

.controller('CuartosCtrl', ['$scope', '$http', 'config', function ($scope, $http, config) {

    $scope.cuartos = [];

    $http.get('/data/cuartos.json').then(
        function(data){ 
            $scope.cuartos = data.data;
        }, function(data){
            console.log(data);
        }
    );
        
}])

.controller('CorreoCtrl', ['$scope', '$http', 'config', 'Dateme', function ($scope, $http, config, Dateme) {

    $scope.email = function(correo){
        console.log(correo);
        if($scope.correo){
            $scope.loader = true;
            $http.post(config.url + 'email', $scope.correo).
              success(function(data, status) {
                if (status == 200) {
                    $scope.correo = {};
                    $scope.loader = false;
                    $scope.enviado = true;
                }else{
                    $scope.loader = false;
                    $scope.errores = true;
                }
              }).
              error(function(data, status) {
                $scope.loader = false;
                $scope.errores = true;
              });
        }
    }
        
}]);