<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/nosotros', 'HomeController@nosotros')->name('nosotros');
Route::get('/tours', 'HomeController@tours')->name('tours');
Route::get('/servicios', 'HomeController@servicios')->name('servicios');
Route::get('/productos', 'HomeController@productos')->name('productos');
Route::get('/contactos', 'HomeController@contactos')->name('contactos');
Route::post('/email', 'HomeController@correo')->name('correo');
Route::get('/correo', function(){ 

    // Info
    $data['email'] = "alvarado@gmail.com";
    $data['entrada'] =  "Fri Aug 26 2016 00:00:00 GMT-0600 (Central America Standard Time)";
    $data['habitacion'] = "Delux";
    $data['nombre'] = "Alvarado";
    $data['nota'] = "Ninguna";
    $data['personas'] =  "1";
    $data['salida'] =  "Fri Aug 26 2016 00:00:00 GMT-0600 (Central America Standard Time)";
    $data['servicio'] = "Tour y Hotel";
    $data['tour'] = "Lago";

    return view('emails.contacto', compact('data')); 
});

